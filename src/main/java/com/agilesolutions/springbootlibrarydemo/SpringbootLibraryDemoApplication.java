package com.agilesolutions.springbootlibrarydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLibraryDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLibraryDemoApplication.class, args);
    }
}
