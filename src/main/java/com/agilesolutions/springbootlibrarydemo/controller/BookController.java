package com.agilesolutions.springbootlibrarydemo.controller;

import com.agilesolutions.springbootlibrarydemo.entity.Book;
import com.agilesolutions.springbootlibrarydemo.entity.Bookcase;
import com.agilesolutions.springbootlibrarydemo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/libraries/{libraryId}/bookcases/{bookcaseId}/books")
    public List<Book> getBooks(@PathVariable String bookcaseId){
        return bookService.getAllBooks(bookcaseId);
    }

    @GetMapping("/libraries/{libraryId}/bookcases/{bookcaseId}/books/{bookId}")
    public Book getBook(@PathVariable String bookId){
        return bookService.getBook(bookId);
    }

    @PostMapping("/libraries/{libraryId}/bookcases/{bookcaseId}/books")
    public void addBook(@RequestBody Book book, @PathVariable String bookcaseId){
        book.setBookcase(new Bookcase(bookcaseId,"","",""));
        bookService.addBook(book);
    }

    @PutMapping("/libraries/{libraryId}/bookcases/{bookcaseId}/books/{bookId}")
    public void updateBook(@RequestBody Book book){
        bookService.updateBook(book);
    }

    @DeleteMapping("/libraries/{libraryId}/bookcases/{bookcaseId}/books/{bookId}")
    public void deletedBook(@PathVariable String bookId){
        bookService.deleteBook(bookId);
    }
}
