package com.agilesolutions.springbootlibrarydemo.controller;

import com.agilesolutions.springbootlibrarydemo.entity.Bookcase;
import com.agilesolutions.springbootlibrarydemo.entity.Library;
import com.agilesolutions.springbootlibrarydemo.service.BookcaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookcaseController {

    @Autowired
    private BookcaseService bookcaseService;

    @GetMapping("/libraries/{libraryId}/bookcases")
    public List<Bookcase> getBookcases(@PathVariable String libraryId){
        return bookcaseService.getAllBookcases(libraryId);
    }

    @GetMapping("/libraries/{libraryId}/bookcases/{id}")
    public Bookcase getBookcase(@PathVariable String id){
        return bookcaseService.getBookcase(id);
    }

    @PostMapping("/libraries/{libraryId}/bookcases")
    public void addBookcase(@RequestBody Bookcase bookcase, @PathVariable String libraryId){
        bookcase.setLibrary(new Library(libraryId,"",""));
        bookcaseService.addBookcase(bookcase);
    }

    @PutMapping("/libraries/{libraryId}/bookcases/{id}")
    public void updateBookcase(@RequestBody Bookcase bookcase, @PathVariable String libraryId, @PathVariable String id){
        bookcase.setLibrary(new Library(libraryId,"",""));
        bookcaseService.updateBookcase(bookcase);
    }

    @DeleteMapping("/libraries/{libraryId}/bookcases/{id}")
    public void deleteBookcase(@PathVariable String id){
        bookcaseService.deleteBookcase(id);
    }
}
