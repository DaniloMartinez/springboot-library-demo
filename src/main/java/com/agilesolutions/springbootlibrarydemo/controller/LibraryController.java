package com.agilesolutions.springbootlibrarydemo.controller;

import com.agilesolutions.springbootlibrarydemo.entity.Library;
import com.agilesolutions.springbootlibrarydemo.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LibraryController {

    @Autowired
    private LibraryService libraryService;

    @GetMapping("/libraries")
    public List<Library> getLibraries(){
        return libraryService.getAllLibraries();
    }

    @GetMapping("/libraries/{id}")
    public Library getLibrary(@PathVariable String id){
        return libraryService.getLibrary(id);
    }

    @PostMapping("/libraries")
    public void addLibrary(@RequestBody Library library){
        libraryService.addLibrary(library);
    }

    @PutMapping("/libraries/{id}")
    public void updateLibrary(@RequestBody Library library, @PathVariable String id){
        libraryService.updateLibrary(id, library);
    }

    @DeleteMapping("/libraries/{id}")
    public void deleteLibrary(@PathVariable String id){
        libraryService.deleteLibrary(id);
    }

}
