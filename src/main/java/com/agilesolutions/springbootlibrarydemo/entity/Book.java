package com.agilesolutions.springbootlibrarydemo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Getter @Setter @NoArgsConstructor
public class Book {

    @Id
    private String id;
    private String name;
    private String description;
    private String author;
    @ManyToOne
    private Bookcase bookcase;

    public Book(String id, String name, String description,String author, String bookcaseId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.author = author;
        this.bookcase = new Bookcase(bookcaseId, "", "","");
    }

}
