package com.agilesolutions.springbootlibrarydemo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Getter @Setter @NoArgsConstructor
public class Bookcase {

    @Id
    private String id;
    private String name;
    private String description;

    @ManyToOne
    private Library library;

    public Bookcase(String id, String name, String description, String libraryId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.library = new Library(libraryId,"","");
    }

}
