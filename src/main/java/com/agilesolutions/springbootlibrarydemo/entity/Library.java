package com.agilesolutions.springbootlibrarydemo.entity;

import lombok.*;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Library {

    @Id
    private @NonNull String id;
    private String name;
    private String description;

}
