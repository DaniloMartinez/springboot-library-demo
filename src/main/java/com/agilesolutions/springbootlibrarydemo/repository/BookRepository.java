package com.agilesolutions.springbootlibrarydemo.repository;

import com.agilesolutions.springbootlibrarydemo.entity.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepository extends CrudRepository<Book, String> {
      public List<Book> findBookByBookcaseId(String id);
}
