package com.agilesolutions.springbootlibrarydemo.repository;

import com.agilesolutions.springbootlibrarydemo.entity.Bookcase;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookcaseRepository extends CrudRepository<Bookcase, String> {
    public List<Bookcase> findBookcasesByLibraryId(String id);
}
