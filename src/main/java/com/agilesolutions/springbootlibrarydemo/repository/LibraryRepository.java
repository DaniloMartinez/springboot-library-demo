package com.agilesolutions.springbootlibrarydemo.repository;

import com.agilesolutions.springbootlibrarydemo.entity.Library;
import org.springframework.data.repository.CrudRepository;

public interface LibraryRepository extends CrudRepository<Library, String> {

}
