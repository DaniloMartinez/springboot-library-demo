package com.agilesolutions.springbootlibrarydemo.service;

import com.agilesolutions.springbootlibrarydemo.entity.Book;
import com.agilesolutions.springbootlibrarydemo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAllBooks(String bookcaseId){
        List<Book> books = new ArrayList<>();
        bookRepository.findBookByBookcaseId(bookcaseId)
            .forEach(books::add);
        return books;
    }

    public Book getBook(String id){
        return bookRepository.findById(id).get();
    }

    public void addBook(Book book){
        bookRepository.save(book);
    }

    public void updateBook(Book book){
        bookRepository.save(book);
    }

    public void deleteBook(String id){
        bookRepository.deleteById(id);
    }
}
