package com.agilesolutions.springbootlibrarydemo.service;

import com.agilesolutions.springbootlibrarydemo.entity.Bookcase;
import com.agilesolutions.springbootlibrarydemo.repository.BookcaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookcaseService {

    @Autowired
    private BookcaseRepository bookcaseRepository;

    public List<Bookcase> getAllBookcases(String libraryId){
        List<Bookcase> bookcases = new ArrayList<>();
        bookcaseRepository.findBookcasesByLibraryId(libraryId)
            .forEach(bookcases::add);
        return bookcases;
    }

    public Bookcase getBookcase(String id){
        return bookcaseRepository.findById(id).get();
    }

    public void addBookcase(Bookcase bookcase){
        bookcaseRepository.save(bookcase);
    }

    public void updateBookcase(Bookcase bookcase){
        bookcaseRepository.save(bookcase);
    }

    public void deleteBookcase(String id){
        bookcaseRepository.deleteById(id);
    }
}
