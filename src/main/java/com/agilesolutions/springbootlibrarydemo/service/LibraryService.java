package com.agilesolutions.springbootlibrarydemo.service;

import com.agilesolutions.springbootlibrarydemo.entity.Library;
import com.agilesolutions.springbootlibrarydemo.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LibraryService {

    @Autowired
    private LibraryRepository libraryRepository;

    public List<Library> getAllLibraries(){
        List<Library> libraries = new ArrayList<>();
        libraryRepository.findAll()
        .forEach(libraries::add);
        return libraries;
    }

    public Library getLibrary(String id){
        return libraryRepository.findById(id).get();
    }

    public void addLibrary(Library library){
        libraryRepository.save(library);
    }

    public void updateLibrary(String libraryId, Library library){
        libraryRepository.save(library);
    }

    public void deleteLibrary(String id){
        libraryRepository.deleteById(id);
    }
}
